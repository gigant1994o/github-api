package com.githug.analizer.github_analizer.service;

import com.githug.analizer.github_analizer.analyzer.model.UserImpact;
import com.githug.analizer.github_analizer.model.response.GitHubCommitResponse;
import com.githug.analizer.github_analizer.model.response.GitHubProjectResponse;
import com.githug.analizer.github_analizer.service.commit.CommitService;
import com.githug.analizer.github_analizer.service.project.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Global App Service which will be used globally in project.
 * Implemented DAO Pattern
 */
@Service
public class AppService implements CommitService, ProjectService {


    @Autowired
    @Qualifier("CommitServiceImpl")
    private CommitService commitService;

    @Autowired
    @Qualifier("ProjectServiceImpl")
    private ProjectService projectService;

    @Override
    public List<GitHubCommitResponse> findCommitsForProject(String ownerName, String projectName) {
        return commitService.findCommitsForProject(ownerName, projectName);
    }

    @Override
    public List<UserImpact> getUsersImpactWithPercentages(List<GitHubCommitResponse> commitResponses) {
        return commitService.getUsersImpactWithPercentages(commitResponses);
    }

    @Override
    public GitHubProjectResponse findProjectByNameLimit100(String projectName) {
        return projectService.findProjectByNameLimit100(projectName);
    }

    @Override
    public GitHubProjectResponse findProject(String name, int page, int count) {
        return projectService.findProject(name,page,count);
    }

    public List<List<Map<Object, Object>>> getCanvasjsChartData(List<GitHubCommitResponse> commitResponses) {
        List<UserImpact> impacts = commitService.getUsersImpactWithPercentages(commitResponses);
        Map<Object, Object> map = null;
        List<List<Map<Object, Object>>> list = new ArrayList<List<Map<Object, Object>>>();
        List<Map<Object, Object>> dataPoints1 = new ArrayList<Map<Object, Object>>();

        for (UserImpact impact:impacts){
            map = new HashMap<Object, Object>();
            map.put("label", impact.getUsername());
            map.put("y", impact.getPercentage());
            dataPoints1.add(map);
        }
        list.add(dataPoints1);
        return list;
    }
}
