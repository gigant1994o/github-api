package com.githug.analizer.github_analizer.service.project;

import com.githug.analizer.github_analizer.model.response.GitHubProjectResponse;
import com.githug.analizer.github_analizer.repo.GithubProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Service working with projects
 */
public class ProjectServiceImpl implements ProjectService{

    @Autowired
    private GithubProjectRepository githubProjectRepository;

    @Override
    public GitHubProjectResponse findProjectByNameLimit100(String projectName) {
        return githubProjectRepository.findProjectByNameLimit100(projectName);
    }

    @Override
    public GitHubProjectResponse findProject(String name, int page, int count) {
        return githubProjectRepository.findProject(name,page,count);
    }
}
