package com.githug.analizer.github_analizer.service.commit;

import com.githug.analizer.github_analizer.analyzer.model.UserImpact;
import com.githug.analizer.github_analizer.model.response.GitHubCommitResponse;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

public interface CommitService {
    List<GitHubCommitResponse> findCommitsForProject(String ownerName, String projectName);
    List<UserImpact> getUsersImpactWithPercentages(List<GitHubCommitResponse> commitResponses);

}
