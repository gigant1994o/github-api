package com.githug.analizer.github_analizer.service.commit;

import com.githug.analizer.github_analizer.analyzer.CommitterPercentageAnalyzer;
import com.githug.analizer.github_analizer.analyzer.model.UserImpact;
import com.githug.analizer.github_analizer.model.response.GitHubCommitResponse;
import com.githug.analizer.github_analizer.repo.GithubCommitRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Service working with commits
 */
public class CommitServiceImpl implements CommitService {

    @Autowired
    private GithubCommitRepository githubCommitRepository;

    @Override
    public List<GitHubCommitResponse> findCommitsForProject(String ownerName, String projectName) {
        return githubCommitRepository.findCommitsForProject(ownerName,projectName);
    }

    @Override
    public List<UserImpact> getUsersImpactWithPercentages(List<GitHubCommitResponse> commitResponses) {
        CommitterPercentageAnalyzer analyzer = new CommitterPercentageAnalyzer(commitResponses);
        return analyzer.analyze();
    }
}
