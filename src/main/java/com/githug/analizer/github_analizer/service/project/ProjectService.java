package com.githug.analizer.github_analizer.service.project;

import com.githug.analizer.github_analizer.model.GithubProject;
import com.githug.analizer.github_analizer.model.response.GitHubProjectResponse;

import java.util.List;

public interface ProjectService {
    GitHubProjectResponse findProjectByNameLimit100(String projectName);

    GitHubProjectResponse findProject(String name, int page, int count);

}
