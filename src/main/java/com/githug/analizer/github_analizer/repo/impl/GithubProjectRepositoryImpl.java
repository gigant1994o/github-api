package com.githug.analizer.github_analizer.repo.impl;

import com.githug.analizer.github_analizer.model.GithubProject;
import com.githug.analizer.github_analizer.model.response.GitHubProjectResponse;
import com.githug.analizer.github_analizer.repo.GithubProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

/**
 * Repository for getting repositories using specified query (name)
 */
public class GithubProjectRepositoryImpl implements GithubProjectRepository{
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public GitHubProjectResponse findProjectByNameLimit100(String name) {
        ResponseEntity<GitHubProjectResponse> forEntity =
                restTemplate.getForEntity(String.format("https://api.github.com/search/repositories?q=%s&per_page=100", name), GitHubProjectResponse.class);
        return forEntity.getBody();
    }

    @Override
    public GitHubProjectResponse findProject(String name, int page, int count) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set( "User-Agent", "request");
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        ResponseEntity<GitHubProjectResponse> forEntity =
                restTemplate.getForEntity(String.format("https://api.github.com/search/repositories?q=%s&page=%d&per_page=%d", name,page,count), GitHubProjectResponse.class,entity);
        return forEntity.getBody();
    }


}
