package com.githug.analizer.github_analizer.repo.impl;

import com.githug.analizer.github_analizer.model.response.GitHubCommitResponse;
import com.githug.analizer.github_analizer.repo.GithubCommitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Repository for getting commits for specified owner and project
 */
public class GithubCommitRepositoryImpl implements GithubCommitRepository {

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<GitHubCommitResponse> findCommitsForProject(String ownerName, String projectName) {
        ParameterizedTypeReference<List<GitHubCommitResponse>> responseType = new ParameterizedTypeReference<List<GitHubCommitResponse>>() {
        };

        try {
            ResponseEntity<List<GitHubCommitResponse>> forEntity =
                    restTemplate.exchange(String.format("https://api.github.com/repos/%s/%s/commits?per_page=100", ownerName, projectName), HttpMethod.GET, null, responseType);
            return forEntity.getBody();

        } catch (HttpClientErrorException exception) {
            return new ArrayList<>();
        }
    }
}
