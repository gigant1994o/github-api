package com.githug.analizer.github_analizer.repo;

import com.githug.analizer.github_analizer.model.GithubProject;
import com.githug.analizer.github_analizer.model.response.GitHubProjectResponse;

import java.util.List;

public interface GithubProjectRepository {
    GitHubProjectResponse findProjectByNameLimit100(String name);
    GitHubProjectResponse findProject(String name,int page,int count);

}
