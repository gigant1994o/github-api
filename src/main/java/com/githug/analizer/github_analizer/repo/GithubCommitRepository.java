package com.githug.analizer.github_analizer.repo;

import com.githug.analizer.github_analizer.model.response.GitHubCommitResponse;

import java.util.List;

public interface GithubCommitRepository {
    List<GitHubCommitResponse> findCommitsForProject(String ownerName, String projectName);
}
