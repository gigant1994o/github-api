package com.githug.analizer.github_analizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class GithubAnalizerApplication extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(GithubAnalizerApplication.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(GithubAnalizerApplication.class, args);
	}
}
