package com.githug.analizer.github_analizer.analyzer;

import com.githug.analizer.github_analizer.analyzer.model.UserImpact;
import com.githug.analizer.github_analizer.model.CommitContent;
import com.githug.analizer.github_analizer.model.response.GitHubCommitResponse;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * Analyzer class for different types of analyzes using commit response from Github API
 */
public class CommitterPercentageAnalyzer {
    private List<GitHubCommitResponse> commits;

    public CommitterPercentageAnalyzer(List<GitHubCommitResponse> commits) {
        this.commits = commits;
    }

    /**
     * method for analyzing all committers impacts
     * @return List of UserImpact objects (username & impact(percentage))
     */
    public List<UserImpact> analyze(){
        List<UserImpact> userImpacts = new ArrayList<>();
        Map<String,List<CommitContent>> data = new HashMap<>();
        for (GitHubCommitResponse commit:commits){
            if (!data.containsKey(commit.getContent().getCommitter().getName())){
                data.put(commit.getContent().getCommitter().getName(),new ArrayList<>(Arrays.asList(commit.getContent())));
            }else {
                data.get(commit.getContent().getCommitter().getName()).add(commit.getContent());
            }
        }
        Iterator<String> userNameIterator = data.keySet().iterator();
        while (userNameIterator.hasNext()){
            String uName = userNameIterator.next();
            userImpacts.add(new UserImpact(uName,new BigDecimal(data.get(uName).size()*100.0/commits.size()).setScale(2, RoundingMode.HALF_UP).doubleValue()));
        }
        return userImpacts;
    }

}
