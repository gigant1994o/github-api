package com.githug.analizer.github_analizer.analyzer.model;

/**
 * class provides model for calculating percentage of commits of user
 */
public class UserImpact {
    private String username;
    private Double percentage;

    public UserImpact(String username, Double percentage) {
        this.username = username;
        this.percentage = percentage;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }
}
