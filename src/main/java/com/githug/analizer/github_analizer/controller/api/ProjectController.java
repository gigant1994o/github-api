package com.githug.analizer.github_analizer.controller.api;

import com.githug.analizer.github_analizer.model.GithubProject;
import com.githug.analizer.github_analizer.model.response.GitHubProjectResponse;
import com.githug.analizer.github_analizer.repo.GithubProjectRepository;
import com.githug.analizer.github_analizer.service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Rest Controller for Github Project (Repository)
 */
@RestController
public class ProjectController {
    @Autowired
    private AppService appService;

    @RequestMapping(value = "/api/repositories", method = RequestMethod.GET)
    public ResponseEntity<GitHubProjectResponse> repositories(@RequestParam(name = "q") String query,@RequestParam(name = "start") int start,@RequestParam(name = "length") int count) {
        start++;
        return new ResponseEntity<>(appService.findProject(query,start/count +1,count), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/repositories100", method = RequestMethod.GET)
    public ResponseEntity<GitHubProjectResponse> repositories100(@RequestParam(name = "q") String query) {
        return new ResponseEntity<>(appService.findProjectByNameLimit100(query), HttpStatus.OK);
    }
}