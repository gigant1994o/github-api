package com.githug.analizer.github_analizer.controller.mvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.githug.analizer.github_analizer.model.response.GitHubCommitResponse;
import com.githug.analizer.github_analizer.model.response.GitHubProjectResponse;
import com.githug.analizer.github_analizer.service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * MVC Controller
 */
@Controller
public class AppController {
    private static final String cookieSplitter = "&&&";
    @Autowired
    private AppService appService;

    @RequestMapping("/")
    public String welcome(Map<String, Object> model) {
        return "index";
    }

    @RequestMapping("/search")
    public String search(@RequestParam(value = "query") String query, Map<String, Object> model) {
        model.put("query", query);
        return "searchResult";
    }

    @RequestMapping("/search/{query}")
    public String searchByBookMark(@PathVariable String query, Map<String, Object> model) {
        model.put("query", query);
        return "searchResult";
    }


    @RequestMapping("/commits/{owner}/{project}")
    public String commits(@PathVariable String owner, @PathVariable String project, Map<String, Object> model) {
        List<GitHubCommitResponse> commitsForProject = appService.findCommitsForProject(owner, project);
        List<List<Map<Object, Object>>> canvasjsDataList = appService.getCanvasjsChartData(commitsForProject);
        model.put("dataPointsList", canvasjsDataList);
        model.put("commits", commitsForProject);
        return "commits";
    }


    @RequestMapping("/openBookmarks")
    public String bookMark(@CookieValue(value = "saved", defaultValue = "no") String savedBookMarks, Map<String, Object> model) {
        if (!savedBookMarks.equals("no")) {
            model.put("bookmarks", savedBookMarks.split(cookieSplitter));
        }
        return "bookmarks";
    }

    @RequestMapping(value = "/addBookmark", method = RequestMethod.GET)
    public String addBookmark(@CookieValue(value = "saved", defaultValue = "no") String savedBookMarks, @RequestParam(name = "q") String query, HttpServletRequest req, HttpServletResponse response) throws JsonProcessingException {
        if (savedBookMarks.equals("no")) {
            response.addCookie(new Cookie("saved", query));
        } else {
            if (!Arrays.asList(savedBookMarks.split(cookieSplitter)).contains(query)) {
                response.addCookie(new Cookie("saved", savedBookMarks + cookieSplitter + query));
            }
        }

        return "redirect:/openBookmarks";
    }

}
