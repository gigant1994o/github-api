package com.githug.analizer.github_analizer.controller.api;

import com.githug.analizer.github_analizer.model.response.GitHubCommitResponse;
import com.githug.analizer.github_analizer.repo.GithubCommitRepository;
import com.githug.analizer.github_analizer.service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Rest Controller for Commits
 */
@RestController
public class CommitController {
    @Autowired
    private AppService appService;

    @RequestMapping(value = "/api/commits/{owner}/{project}",method = RequestMethod.GET)
    public ResponseEntity<List<GitHubCommitResponse>> repositories(@PathVariable String owner, @PathVariable String project) {
        return new ResponseEntity<>(appService.findCommitsForProject(owner,project), HttpStatus.OK);
    }
}
