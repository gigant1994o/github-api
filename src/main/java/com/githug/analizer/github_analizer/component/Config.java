package com.githug.analizer.github_analizer.component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.githug.analizer.github_analizer.repo.GithubCommitRepository;
import com.githug.analizer.github_analizer.repo.GithubProjectRepository;
import com.githug.analizer.github_analizer.repo.impl.GithubCommitRepositoryImpl;
import com.githug.analizer.github_analizer.repo.impl.GithubProjectRepositoryImpl;
import com.githug.analizer.github_analizer.service.commit.CommitService;
import com.githug.analizer.github_analizer.service.project.ProjectService;
import com.githug.analizer.github_analizer.service.commit.CommitServiceImpl;
import com.githug.analizer.github_analizer.service.project.ProjectServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Component
public class Config {

    private ObjectMapper jacksonObjectMapper() {
        return new ObjectMapper()
                .setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
        jsonMessageConverter.setObjectMapper(jacksonObjectMapper());
        messageConverters.add(jsonMessageConverter);
        restTemplate.setMessageConverters(messageConverters);

        return restTemplate;
    }


    @Bean
    public GithubProjectRepository getProjectRepository() {
        return new GithubProjectRepositoryImpl();
    }

    @Bean
    public GithubCommitRepository getCommitRepository() {
        return new GithubCommitRepositoryImpl();
    }

    @Bean(name="CommitServiceImpl")
    public CommitService getCommitService() {
        return new CommitServiceImpl();
    }

    @Bean(name="ProjectServiceImpl")
    public ProjectService getProjectService() {
        return new ProjectServiceImpl();
    }



}