package com.githug.analizer.github_analizer.model;

/**
 * Model describing Owner of repository
 */
public class Owner {
    private String login;
    private int id;
    private String avatarUrl;
    private String url;

    public Owner(){

    }
    public Owner(String login, int id, String avatarUrl, String url) {
        this.login = login;
        this.id = id;
        this.avatarUrl = avatarUrl;
        this.url = url;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
