package com.githug.analizer.github_analizer.model.response;

import com.githug.analizer.github_analizer.model.GithubProject;

import java.util.List;

/**
 * Response Model for Projects call
 */
public class GitHubProjectResponse{
    private Long total_count;
    private List<GithubProject> items;


    public List<GithubProject> getItems() {
        return items;
    }

    public void setItems(List<GithubProject> items) {
        this.items = items;
    }

    public Long getTotal_count() {
        return total_count;
    }

    public void setTotal_count(Long total_count) {
        this.total_count = total_count;
    }
}
