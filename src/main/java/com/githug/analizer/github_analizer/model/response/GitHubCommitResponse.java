package com.githug.analizer.github_analizer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.githug.analizer.github_analizer.model.CommitContent;

/**
 * Response Model for Commits call
 */
public class GitHubCommitResponse {

    @JsonProperty("commit")
    private CommitContent content;

    public CommitContent getContent() {
        return content;
    }

    public void setContent(CommitContent content) {
        this.content = content;
    }
}
