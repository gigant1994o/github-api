package com.githug.analizer.github_analizer.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model for Commit Details
 */
public class CommitContent {
    @JsonProperty("committer")
    private Committer committer;
    @JsonProperty("message")
    private String message;

    public CommitContent(Committer committer, String message) {
        this.committer = committer;
        this.message = message;
    }

    public CommitContent() {
    }

    public Committer getCommitter() {
        return committer;
    }

    public void setCommitter(Committer committer) {
        this.committer = committer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
