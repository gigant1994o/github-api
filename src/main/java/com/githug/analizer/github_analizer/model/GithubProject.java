package com.githug.analizer.github_analizer.model;

/**
 * Model describing github repository
 */
public class GithubProject {
    private Long id;
    private String name;
    private Owner owner;
    private String url;

    public GithubProject() {

    }

    public GithubProject(Long id, String name, Owner owner, String url) {
        this.id = id;
        this.name = name;
        this.owner = owner;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
