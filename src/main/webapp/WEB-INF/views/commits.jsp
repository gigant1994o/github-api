<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#commits').DataTable();
            $('#impacts').DataTable();
        });
    </script>
    <script type="text/javascript">
        window.onload = function() {

            var dps = [[]];
            var chart = new CanvasJS.Chart("chartContainer", {
                theme: "light2", // "light1", "dark1", "dark2"
                exportEnabled: true,
                animationEnabled: true,
                title: {
                    text: "Commit diagram"
                },
                data: [{
                    type: "pie",
                    showInLegend: "true",
                    legendText: "{label}",
                    yValueFormatString: "#,###\"%\"",
                    indexLabelFontSize: 16,
                    indexLabel: "{label} - {y}",
                    dataPoints: dps[0]
                }]
            });

            var yValue;
            var label;

            <c:forEach items="${dataPointsList}" var="dataPoints" varStatus="loop">
            <c:forEach items="${dataPoints}" var="dataPoint">
            yValue = parseFloat("${dataPoint.y}");
            label = "${dataPoint.label}";
            dps[parseInt("${loop.index}")].push({
                label : label,
                y : yValue,
            });
            </c:forEach>
            </c:forEach>

            chart.render();
        }
    </script>
    <title>Commits</title>

</head>
<body>
<div style="margin-right:100px;margin-left:100px">
    <h1>Search result page</h1>
    <h2><a href="/">Search Again</a></h2>

    <h2>Impacts</h2>

    <div id="chartContainer" style="height: 370px; width: 100%;"></div>
    <h2>All commits</h2>
    <table id="commits" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>Name</th>
            <th>Message</th>
            <th>Email</th>
            <th>Date</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach items="${commits}" var="c">
            <tr>
                <td>${c.content.committer.name}</td>
                <td>${c.content.message}</td>
                <td>${c.content.committer.email}</td>
                <td>${c.content.committer.date}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>