<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <title>Bookmarks</title>

    <script>
        $(document).ready(function () {
            $('#result').DataTable();
        });
    </script>
</head>
<body>
<div style="margin-right:100px;margin-left:100px">
    <h1>Bookmarks page</h1>
    <h2><a href="/">Search Again</a></h2>

    <table id="result" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>Name</th>
        </tr>
        </thead>
        <tbody>

        <c:forEach items="${bookmarks}" var="b">
            <tr>
                <td><a href="/search/${b}">${b}</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>