<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <title>Search Result</title>
    <style>

    </style>
    <script>
        $(document).ready(function () {
            $('#result').DataTable({
                serverSide: true,
                processing: true,
                paging: true,
                ajax: {
                    url: '/api/repositories?q=${query}',
                    dataFilter: function (data) {
                        var json = jQuery.parseJSON(data);
                        json.recordsTotal = json.total_count;
                        json.recordsFiltered = json.total_count;
                        json.data = json.items;

                        return JSON.stringify(json); // return JSON string
                    }
                },
                columns: [
                    {
                        data: "id",
                        render: function (data, type, row, meta) {
                            data = '<a href="/commits/' + row.owner.login +'/'+row.name+ '">' + data + '</a>';


                            return data;
                        }
                    },
                    {data: "name"},
                    {data: "owner.login"},
                    {data: "url"}
                ]
            });
        });
    </script>
</head>
<body>
<div style="margin-right:100px;margin-left:100px">
    <h1>Search result page</h1>
    <h2><a href="/">Search Again</a></h2>
    <h2 align="right"><a href="/addBookmark?q=${query}">save in bookmarks</a></h2>


    <table id="result" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Author</th>
            <th>url</th>
        </tr>
        </thead>
    </table>
</div>
</body>
</html>